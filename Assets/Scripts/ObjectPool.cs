﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public static ObjectPool singleton;
    public GameObject prefabOggetto;
    public List<GameObject> instanzeOggetto;
    public int instanzeDaCreare = 10;


    void Awake()
    {
        if (singleton && singleton != this)
        {
            Destroy(gameObject);
            return;
        }
        singleton = this;
    }

    void Start()
    {
        instanzeOggetto = new List<GameObject>();
        GameObject tmp;
        for (int i = 0; i < instanzeDaCreare; i++)
        {
            InstanziaOggetto(out tmp);
        }
    }

    public GameObject GetOggetto()
    {
        for (int i = 0; i < instanzeDaCreare; i++)
        {
            if (instanzeOggetto[i].activeInHierarchy) { continue; }
            return instanzeOggetto[i];
        }
        GameObject nuovoOggetto;
        InstanziaOggetto(out nuovoOggetto);
        return nuovoOggetto;
    }

    void InstanziaOggetto(out GameObject tmp)
    {
        tmp = Instantiate(prefabOggetto);

        //qui possiamo inserire l'inizializzazione

        tmp.SetActive(false);
        instanzeOggetto.Add(tmp);
    }
}
