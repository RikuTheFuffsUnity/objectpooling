﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OggettoDelPool : MonoBehaviour
{
    // Start is called before the first frame update
    void OnEnable()
    {
        StartCoroutine(DisattivaDopo(5));
    }

    IEnumerator DisattivaDopo(float tempo)
    {
        yield return new WaitForSeconds(tempo);
        gameObject.SetActive(false);
    }
}
